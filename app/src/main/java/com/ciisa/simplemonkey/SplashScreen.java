package com.ciisa.simplemonkey;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private final int TIMEOUT = 2000;

    private SharedPreferences preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();  // OCULTA LA APPBAR DEL SPLASH
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);

        preferences = getSharedPreferences("userData", Context.MODE_PRIVATE);
        final boolean isUserActive = preferences.contains("uid");

        new Handler().postDelayed(new Runnable(){
            public void run(){
                Intent intent;
                if(isUserActive) {
                    intent = new Intent(SplashScreen.this, MainActivity.class);
                } else {
                    intent = new Intent(SplashScreen.this, AccountLogin.class);
                }
                startActivity(intent);
                finish();
        };
    }, TIMEOUT);

    }
}





