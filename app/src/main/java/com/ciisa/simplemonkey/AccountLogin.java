package com.ciisa.simplemonkey;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ciisa.simplemonkey.utils.InputValidator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AccountLogin extends AppCompatActivity {

    TextInputLayout tilEmail, tilPassword;
    String strEmail,strPass;
    Button btnLogin;
    TextView tvRegister;
    SharedPreferences preferences;

    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_login);

        getSupportActionBar().hide();  // OCULTA LA APPBAR DEL SPLASH

        // REFERENCIAS
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvRegister = findViewById(R.id.tvRegister);

        preferences = getSharedPreferences("userData", Context.MODE_PRIVATE);

        if(preferences.contains("token")) {
            Intent intent = new Intent(AccountLogin.this, MainActivity.class);
            startActivity(intent);
        }

        // TEXTVIEW REDIRECT A REGISTRO USUARIO
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), AccountRegister.class);
            startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser(tilEmail, tilPassword);
            }
        });


    }

    private void loginUser(final TextInputLayout tilEmail, final TextInputLayout tilPassword) {
        String email = tilEmail.getEditText().getText().toString();
        String password = tilPassword.getEditText().getText().toString();
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    InputValidator inputValidator = new InputValidator(AccountLogin.this);

                    inputValidator.isEmail(tilEmail);
                    inputValidator.isRequired(tilPassword);

                    String id = firebaseAuth.getCurrentUser().getUid();
                    SharedPreferences.Editor preferencesEditor = preferences.edit();
                    preferencesEditor.putString("uid", id);
                    preferencesEditor.apply();
                    Intent intent = new Intent(AccountLogin.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(AccountLogin.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    tilPassword.getEditText().setText("");
                    tilPassword.setError(getString(R.string.login_error));
                }
            }
        });
    }

}
