package com.ciisa.simplemonkey.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private String id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Character gender;
    private Date birth;
    private int country;

    private User(Builder builder) {
        this.id = builder.id;
        this.email = builder.email;
        this.password = builder.password;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.gender = builder.gender;
        this.birth = builder.birth;
        this.country = builder.country;
    }

    public static class Builder {
        private String id;
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private Character gender;
        private Date birth;
        private int country;

        public Builder(@Nullable String id, String email, String firstName, String lastName) {
            this.id = id;
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public User.Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public User.Builder withGender(Character gender) {
            this.gender = gender;
            return this;
        }

        public User.Builder withBirth(Date birth) {
            this.birth = birth;
            return this;
        }

        public User.Builder withCountry(int country) {
            this.country = country;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }
}
