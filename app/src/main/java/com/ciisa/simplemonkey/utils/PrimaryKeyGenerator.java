package com.ciisa.simplemonkey.utils;

public class PrimaryKeyGenerator {
    public static String generate(String uid) {
        return uid + System.currentTimeMillis() / 1000L;
    }
}
